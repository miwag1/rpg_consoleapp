package model.equipment;

public enum WeaponType {
    AXES,
    BOWS,
    DAGGERS,
    HAMMERS,
    STAFFS,
    SWORDS,
    WANDS;
}
