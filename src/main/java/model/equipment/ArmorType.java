package model.equipment;

public enum ArmorType {
    CLOTH,
    LEATHER,
    MAIL,
    PLATE;
}
