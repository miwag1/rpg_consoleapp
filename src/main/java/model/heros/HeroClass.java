package model.heros;

public enum HeroClass {
    MAGE,
    RANGER,
    ROGUE,
    WARRIOR;

    private String heroClass;

    public String getHeroClass(){
        return heroClass;
    }

}
